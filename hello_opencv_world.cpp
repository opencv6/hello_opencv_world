#include <iostream>
#include <opencv2/opencv.hpp>

int main()
{
  std::cout << "Hello OpenCV World" << std::endl;
  cv::Mat image = cv::Mat::zeros(cv::Size(640,480), CV_8UC3);
  cv::circle(image, cv::Point(240,320), 100, cv::Scalar(0,255,255));
  cv::imshow("Hello OpenCV World", image);
  cv::imwrite("hello.png", image);
  cv::waitKey(0); // press a key to continue
  return 0;
}
